package com.wsti.sortcomparison;

import com.wsti.sortcomparison.domain.*;
import dnl.utils.text.table.TextTable;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

public class SortComparisonApplication {

    private static final String[] COLUMNS = {"Algorithm", "Elements", "Duration avg (ms)"};
    private static final Random RANDOM = new Random();

    public static void main(String[] args) {
        List<Result> results = new ArrayList<>();

        for (SortStrategy sortStrategy : getSortStrategies()) {
            for (int dataSize : Arrays.asList(10, 100, 1000, 10000)) {

                List<Integer> unsortedData = generateData(dataSize);
                Duration duration = executeSortingTest(sortStrategy, unsortedData);

                double milliSeconds = (double) duration.toNanos() / 1000000;
                results.add(new Result(sortStrategy.getAlgorithmName(), dataSize, milliSeconds));
            }
        }
        String[][] sortedResults = results.stream()
                .sorted(Comparator
                        .comparing((Result result) -> result.size)
                        .thenComparing(result -> result.duration))
                .map(Result::toArray)
                .toArray(String[][]::new);

        TextTable table = new TextTable(COLUMNS, sortedResults);
        table.printTable();
    }

    private static List<SortStrategy> getSortStrategies() {
        return Arrays.asList(
                new BubbleSortStrategy(),
                new InsertionSortStrategy(),
                new SelectionSortStrategy(),
                new QuickSortStrategy(),
                new JavaSortStrategy());
    }

    private static List<Integer> generateData(int size) {
        List<Integer> data = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            data.add(RANDOM.nextInt(1000) + 1);
        }
        return data;
    }

    private static Duration executeSortingTest(SortStrategy sortStrategy, List<Integer> unsortedData) {
        Duration duration = Duration.ZERO;
        for (int i = 0; i < 10; i++) {
            LocalDateTime startTime = LocalDateTime.now();
            List<Integer> sortedData = sortStrategy.sort(unsortedData);
            duration = duration.plus(Duration.between(startTime, LocalDateTime.now()));

            if (!isSorted(sortedData)) {
                throw new IllegalStateException("Data are not sorted, used algorithm: " + sortStrategy.getAlgorithmName());
            }
        }
        return duration.dividedBy(10);
    }

    private static boolean isSorted(List<Integer> sortedData) {
        return sortedData.stream()
                .sorted()
                .collect(Collectors.toList())
                .equals(sortedData);
    }

    private static class Result {
        private String algorithm;
        private Integer size;
        private Double duration;

        public Result(String algorithm, Integer size, Double duration) {
            this.algorithm = algorithm;
            this.size = size;
            this.duration = duration;
        }

        public String[] toArray() {
            return new String[] {algorithm, String.valueOf(size), String.valueOf(duration)};
        }
    }
}
