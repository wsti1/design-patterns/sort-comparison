package com.wsti.sortcomparison.domain;

import java.util.ArrayList;
import java.util.List;

public class InsertionSortStrategy implements SortStrategy {

    @Override
    public String getAlgorithmName() {
        return "InsertionSort";
    }

    @Override
    public List<Integer> sort(List<Integer> source) {
        List<Integer> target = new ArrayList<>(source);

        for (int j = 1; j < target.size(); j++) {
            int value = target.get(j);
            int i = j - 1;

            while ((i > -1) && (target.get(i) > value)) {
                target.set((i + 1), target.get(i));
                i--;
            }
            target.set((i + 1), value);
        }
        return target;
    }
}
