package com.wsti.sortcomparison.domain;

import java.util.List;

public interface SortStrategy {

    String getAlgorithmName();

    List<Integer> sort(List<Integer> data);
}
