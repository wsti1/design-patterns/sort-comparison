package com.wsti.sortcomparison.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class JavaSortStrategy implements SortStrategy {

    @Override
    public String getAlgorithmName() {
        return "JavaSort";
    }

    @Override
    public List<Integer> sort(List<Integer> source) {
        List<Integer> target = new ArrayList<>(source);

        Collections.sort(target);
        return target;
    }
}
