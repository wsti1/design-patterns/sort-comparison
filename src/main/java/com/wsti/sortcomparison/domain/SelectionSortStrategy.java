package com.wsti.sortcomparison.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SelectionSortStrategy implements SortStrategy {

    @Override
    public String getAlgorithmName() {
        return "SelectionSort";
    }

    @Override
    public List<Integer> sort(List<Integer> source) {
        List<Integer> target = new ArrayList<>(source);

        for (int i = 0; i < (target.size() - 1); i++) {
            int index = i;

            for (int j = i + 1; j < target.size(); j++){
                if (target.get(j) < target.get(index)){
                    index = j;
                }
            }
            Collections.swap(target, index, i);
        }
        return target;
    }
}
