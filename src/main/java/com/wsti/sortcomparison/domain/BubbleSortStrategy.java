package com.wsti.sortcomparison.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BubbleSortStrategy implements SortStrategy {

    @Override
    public String getAlgorithmName() {
        return "BubbleSort";
    }

    @Override
    public List<Integer> sort(List<Integer> source) {
        List<Integer> target = new ArrayList<>(source);

        for (int i = 0; i < target.size(); i++) {
            for (int j = 1; j < (target.size() - i); j++) {

                if (target.get(j - 1) > target.get(j)) {
                    Collections.swap(target, j, (j - 1));
                }
            }
        }
        return target;
    }
}
