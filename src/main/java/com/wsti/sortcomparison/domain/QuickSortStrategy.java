package com.wsti.sortcomparison.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class QuickSortStrategy implements SortStrategy {

    @Override
    public String getAlgorithmName() {
        return "QuickSort";
    }

    @Override
    public List<Integer> sort(List<Integer> source) {
        List<Integer> target = new ArrayList<>(source);
        return quickSort(target, 0, (target.size() - 1));
    }

    private List<Integer> quickSort(List<Integer> data, int lowerIndex, int higherIndex) {
        int i = lowerIndex;
        int j = higherIndex;
        int pivot = data.get(lowerIndex+(higherIndex-lowerIndex)/2);

        while (i <= j) {
            while (data.get(i) < pivot) {
                i++;
            }
            while (data.get(j) > pivot) {
                j--;
            }
            if (i <= j) {
                Collections.swap(data, i, j);
                i++;
                j--;
            }
        }
        if (lowerIndex < j) {
            quickSort(data, lowerIndex, j);
        }
        if (i < higherIndex) {
            quickSort(data, i, higherIndex);
        }
        return data;
    }
}
